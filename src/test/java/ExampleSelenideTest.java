import Pages.BasePage;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static Pages.BasePage.*;
import static Pages.LoginPage.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static helpers.TestData.*;

public class ExampleSelenideTest extends BasePage {
    @Test(description = "Авторизация пользователя при введении правильного логина и пароля")
    public void Login_with_correct_username_and_password(){
        open(LoginUrl);
        FillLoginPageFields(ValidUsername,ValidPassword);
        clickButtonJS(SubmitButton);
        CheckElementsDisplayed(LoginMessage,WelcomeMessage,LogoutButton);
    }
    @Test(description = "Неудачная попытка авторизации при введении неправильного логина и правильного пароля")
    public void Login_with_incorrect_username(){
        open(LoginUrl);
        FillLoginPageFields(InvalidUsername,ValidPassword);
        clickButtonJS(SubmitButton);
        CheckElementsDisplayed(UsernameInvalidMessage);
    }

    @Test(description = "Неудачная попытка авторизации при введении правильного логина и правильрного пароля")
    public void Login_with_incorrect_password(){
        open(LoginUrl);
        FillLoginPageFields(ValidUsername,InvalidPassword);
        clickButtonJS(SubmitButton);
        CheckElementsDisplayed(PasswordInvalidMessage);
    }
}
