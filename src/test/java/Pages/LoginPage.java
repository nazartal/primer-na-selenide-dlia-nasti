package Pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.*;

public class LoginPage {
    public static SelenideElement Username = $("#username");
    public static SelenideElement Password= $("#password");

    @FindBy(className = "radius")
    public static SelenideElement SubmitButton = $(".radius");
    public static SelenideElement LoginMessage = $(By.xpath("//div[contains(text(),'You logged into a secure area!')]"));
    public static SelenideElement WelcomeMessage = $(By.xpath("//h4[contains(text(),'Welcome to the Secure Area. When you are done click logout below.')]"));
    public static SelenideElement UsernameInvalidMessage = $(By.xpath("//div[contains(text(),'Your username is invalid!')]"));
    public static SelenideElement PasswordInvalidMessage = $(By.xpath("//div[contains(text(),'Your password is invalid!')]"));
    public static SelenideElement LogoutButton = $(By.linkText("Logout"));

    public static void FillLoginPageFields(String username, String password) {
        Username.setValue("");
        Username.setValue(username);
        Password.setValue("");
        Password.setValue(password);
    }
}
