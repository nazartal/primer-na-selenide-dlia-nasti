package Pages;

import com.codeborne.selenide.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.util.Collections;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BasePage {
    @BeforeMethod
    public static void beforeMethod() {
        Configuration.browser = myChromeBrowserClass.class.getName();
        Configuration.startMaximized = true;
        System.out.println("LocalWebDriver");
        Configuration.reportsFolder = "target/reports";
        Configuration.screenshots = false;
    }
    @AfterMethod
    public void afterMethod() {
        closeWebDriver();
    }
    public static void clickButtonJS(SelenideElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) getWebDriver();
        executor.executeScript("arguments[0].click();", element);
    }
    public static void pressEnterForElement (SelenideElement element) {

        element.sendKeys(Keys.ENTER);
    }

    public static void CheckElementsDisplayed(SelenideElement... list) {
        for (SelenideElement element: list) {
            element.shouldBe(Condition.visible);
        }
    }
}
